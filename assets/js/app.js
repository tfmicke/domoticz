
var config = {
    api_uri: "http://domoticz.malarstrom.nu:8080",
    api_query: "/json.htm?type=devices&jsoncallback=?&filter=all",
    api_plan: "2",
    api_command_uri: "/json.htm?type=command&jsoncallback=?&passcode=Magicnet-7310&param=",
    gui_refresh_rate: 3000, 
};


var devicemap = [   // idx, Value to grab, Label to show, name of data, icon, switchable?, on_state, command
        ['9','Temp','Temp','device91', 'wb_sunny', false, '',''],
        ['9','Humidity','Fuktighet','device92', 'beach_access', false, '',''],
        ['7','Data','Nivå','device7', 'local_florist', false, '',''],
        ['123','Data','Garageport 1','device123', 'directions_car', true, 'On','switchlight'],
        ['135','Data','Garageport 2','device135', 'directions_car', true, 'On','switchlight'],
        ['19','Data','Ytterdörr 1','device19', 'lightbulb_outline', true, 'On', 'switchlight'],
        ['127','Data','Sovrum','device127', 'lightbulb_outline', true, 'On','switchlight'],
        ['26','Data','Sov John','device26', 'lightbulb_outline', true, 'On','switchlight'],
        ['23','Data','Tvättstuga','device23', 'lightbulb_outline', true, 'On','switchlight'],
        ['407','Data','Vardagsrum 01','device407', 'lightbulb_outline', true, 'On','switchlight'],
        ['21','Data','Vardagsrum 02','device21', 'lightbulb_outline', true, 'On','switchlight'],
        ['411','Data','LUA Dummy','device411', 'beach_access', false, 'Open',''],

];



app = {

    doingAjax: false,

    // Send Switch command
    switchToggle: function(idx, params, cmd) {

        var scope = this;

        // Prevent multiple requests at once
        if ( scope.doingAjax ) { 
            return;
        }

        // Start the request
        scope.doingAjax = true;

        $.getJSON( config.api_uri + config.api_command_uri + params + '&idx=' + idx + '&switchcmd=' + cmd, function( data ) {
            scope.doingAjax = false;
            console.log(data);
            clearInterval($.timer);
            scope.start();
        });

    },

    // Communicate with domoticz
    start: function() {

        // Empty timer
        clearInterval($.timer);

        var device = {};    // Single device
        var devices = [];   // Array of devices with data
        var datacards = []; // Array of html markup and device data

        // Load JSON data over the API
        $.getJSON( config.api_uri + config.api_query + config.api_plan, function( data ) {
            // Got values in the data.result object?
            if ( typeof data.result != 'undefined' ) {
                // Loop over all devices in data
                $.each( data.result, function( index, item ) {
                    // Loop over each device attribute in data and map with devicemap
                    for( var i = 0, len = devicemap.length; i < len; i++ ) {
                        // Match idx from data with devicemap and set corresponding values
                        if ( item.idx === devicemap[i][0] ) {
                            var device_armed = ( devicemap[i][6] == item.Data ? true : false );
                            device = {
                                idx: item.idx, 
                                type: item.Type,
                                data: item.Data,
                                value: item[devicemap[i][1]],
                                text: devicemap[i][2],
                                name: devicemap[i][3],
                                icon: devicemap[i][4],
                                updated: item.LastUpdate,
                                switchable: devicemap[i][5],
                                armed: device_armed,
                                params: devicemap[i][7],
                            }

                            //console.log(device);
                            // Prevent adding duplicate indexes (termo and termo, port and port)
                            if ( typeof devices[device.name] === 'undefined' ) {
                                devices[device.name] = device;

                                // Add toggle switch or not
                                if ( true == device.switchable ) {

                                    // Pretend Garage is always closed to always send On!
                                    if ( $.inArray(device.idx, [123,135] ) ){
                                        device.armed = false;
                                    }

                                    element = '<div id="' + device.name + '" class="col-lg-3 col-md-4 col-sm-4"> \
                                                    <div class="card card-stats"> \
                                                    <div class="card-header" data-background-color="green"> \
                                                        <i class="material-icons">'+ device.icon +'</i> \
                                                    </div> \
                                                    <div class="card-content"> \
                                                        <p class="category">' + device.text + ' ('+device.idx+')</p> \
                                                        <h3 class="title">' + device.value + '</h3> \
                                                    </div> \
                                                    <div class="card-footer"><a href="#" class="switch-trigger" data-idx=' + device.idx + ' data-cmd="'+(device.armed ? 'Off' : 'On')+'" data-params="'+device.params+'"> \
                                                        <div class="stats">\
                                                            <div class="togglebutton switch"> \
                                                                <label>\
                                                                    <input type="checkbox" '+ (device.armed ? 'checked' : 'unchecked') +'>\
                                                                        <span class="toggle"></span>\
                                                                    </label>\
                                                            </div>\
                                                        </div></a> \
                                                    </div></div></div>' 
                                } else {

                                    element = '<div id="' + device.name + '" class="col-lg-3 col-md-4 col-sm-4"> \
                                                    <div class="card card-stats"> \
                                                    <div class="card-header" data-background-color="green"> \
                                                        <i class="material-icons">'+ device.icon +'</i> \
                                                    </div> \
                                                    <div class="card-content"> \
                                                        <p class="category">' + device.text + '</p> \
                                                        <h3 class="title">' + device.value + '</h3> \
                                                    </div> \
                                                    <div class="card-footer"> \
                                                        <div class="stats">\
                                                            <i class="material-icons">update</i> '+ device.updated +' </div> \
                                                    </div></div></div>' 
                                }

                                // Add data card
                                datacards[i] = element ;
                            };

                        };
                    }
                });
                // Finally! Print data cards =)
                $('#data-cards').html(datacards);
                // Attach click function to switches
                $("a.switch-trigger").click(function() {
                    // Values picked up from the markup
                    app.switchToggle( $(this).attr('data-idx'), $(this).attr('data-params'), $(this).attr('data-cmd'));             
                });

            }
        });
        
        // Start timer that will refresh the GUI/data
        $.timer = setInterval(app.start, config.gui_refresh_rate);

    }

}

$(function() { 
    app.start();
});



